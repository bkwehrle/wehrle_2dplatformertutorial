﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // Start is called before the first frame update
    float timer = 151.0f;
    int seconds = 0;
    int minutes = 0;
    public int day = 1;
    public int wood = 0;
    public Text secondText;
    public Text minuteText;
    public Text dayText;
    public Text woodText;
    void Start()
    {
        
    }
    //My timer class does the following:
    //it counts down from 2:30 and increments day by one each time
    //it updates the UI.
    //it keeps track of resources.
    // Update is called once per frame
    void Update()
    {
        //pretty by the books implementation of a timer. using mod for seconds 
        //and floor for my minutes.
        timer -= Time.deltaTime;
        seconds = (int) timer % 60;
        minutes = (int) Mathf.Floor(timer / 60);
        if (timer <= 0.2){
            timer = 151.0f;
            day += 1;
        }
        if (seconds < 10){
            secondText.text = "0"+seconds.ToString();
        }
        else 
        {
            secondText.text = seconds.ToString();
        }
        minuteText.text = minutes.ToString() + ":";
        dayText.text = "Day "+ day.ToString();
        woodText.text = "Wood: "+wood.ToString();
        //print("minutes:" + minutes.ToString());
        //print("seconds:" + seconds.ToString());
        
    }
    public void woodPlusOne(){
        wood += 1;
    }
}
