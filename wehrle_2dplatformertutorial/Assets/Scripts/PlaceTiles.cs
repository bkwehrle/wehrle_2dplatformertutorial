﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlaceTiles : MonoBehaviour
{

    //tilemap is my platform tilemap
    public Tilemap tilemap;
    //backtilemap is my stone background
    public Tilemap backtilemap;
    //grasstilemap is my grass background
    public Tilemap grasstilemap;
    public Tile stone;
    public RuleTile tile;
    public RandomTile grass;
    public GameObject crate;
    

    
    //vector3 but without floats
    Vector3Int previous_position;
    // Start is called before the first frame update
    void Awake()
    {   
        //ask Ian why my map is no longer random : <
        
        //Okay so my approach for generating the map in general was the following:
        //generate the map left to right with the height of the ground either going up 1
        //going down 1 or staying the same.
        //since then i have added some variation with my cliffChance to make it possible
        //for it to go up by two. 
        // get current grid location
        //
        //hardcoded the start of the map in there.
        Vector3Int currentCell = new Vector3Int(-87, -6, 0);
        int maxHeight = 6;
        int heigthOfLastColumn = 4;
        int mapLength = 171;
        bool changed = false;
        //NEW: made it so every 25 blocks a few columns generate structures
        int structureChance = 0;
        int clusterTime = Random.Range(1,21);
        // add one in a direction (you'll have to change this to match your directional control)
        for (int i = 0; i < mapLength; i++){
            for(int j = 0; j < heigthOfLastColumn; j++){

                //grass placement
                if (j==heigthOfLastColumn-1){
                    currentCell.y += 1;
                    grasstilemap.SetTile(currentCell, grass);
                    currentCell.y -= 1;
                }

                //structure generation
                if (j==heigthOfLastColumn-1&& clusterTime > 25){
                    structureChance = Random.Range(0,2);
                    if (structureChance == 0){
                        backtilemap.SetTile(currentCell, stone);
                        currentCell.y += 1;
                        backtilemap.SetTile(currentCell, stone);
                        currentCell.y += 1;
                        backtilemap.SetTile(currentCell, stone);
                        currentCell.y += 1;
                        tilemap.SetTile(currentCell, stone);
                        currentCell.y -= 3;
                    }
                    
                }
                //nested for's to get my x and y.
                tilemap.SetTile(currentCell, tile);
                currentCell.y += 1; 
                //start at the bottom left corner and go up
            }
            clusterTime += 1;
            if (clusterTime > 31){
                clusterTime = 0;
            }
            currentCell.y -= heigthOfLastColumn;
            //go back down
            currentCell.x += 1;
            //go to the right one
            //now i got a new bottom left corner : )
            

            //this is kinda messy now.
            //randNum is a number 1 to 3.
            //if 1 our next columns height goes down
            //if 2 it goes up
            //if 3 it stays
            //
            //if 3 it also has a 1 in 3 chance to be a "cliff"
            //also it will never change twice in a row (to smooth the hills slightly)
            //this is thanks to the changed boolean.
            int randNum  = Random.Range(1, 4); 
            int cliffChance  = Random.Range(1, 4); 
            if (randNum == 1 && changed == false){
                heigthOfLastColumn -= 1;   
                changed = true;
            }
            else if (randNum == 2  && changed == false){
                heigthOfLastColumn += 1;   
                changed = true;
            }
            else if (cliffChance == 1  && changed == false){
                heigthOfLastColumn += 2;   
                changed = true;
            }
            else
            {
                changed = false;
            }
            if (heigthOfLastColumn <= 1){
                heigthOfLastColumn = 2;
            }
            if (heigthOfLastColumn > 6){
                heigthOfLastColumn = maxHeight;
            }
        }
        
        
    }
    public void PlaceCrate(Vector3 vec3){
        vec3.x -= .9f;
        vec3.y += 1.2f;
        Instantiate(crate,vec3,Quaternion.identity);
    }
}
