﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScroll : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cc1;
    public GameObject cc2;
    public GameObject cc3;
    GameObject c;
    float timer = 0f;
    float nextcloud = 10.0f;
    float cloudLifeSpan = 1000f;
    void Awake()
    {
        for (float i = -79f; i<80f; i += nextcloud){
            makeCustomCloud(i);
            nextcloud = Random.Range(4.2f,6.6f);
        }
    }
    void Start(){
        nextcloud = 16.0f;
    }

    // Update is called once per frame
    void Update()
    {  
        timer += Time.deltaTime;
        //print(timer);
        if (timer > nextcloud){
            makeCloud();
            timer = 0f;
            nextcloud = Random.Range(14f,20f);
        }
        //2.9 - 4.25
    }
    void makeCloud(){
        int chance = Random.Range(0,4);
        float height = Random.Range(1.8f,4.25f);
        if (chance == 0){
            c = Instantiate(cc1,new Vector3 (-88, height, 0),Quaternion.identity);
            Destroy(c,cloudLifeSpan);
        }
        else if (chance == 1){
            c = Instantiate(cc2,new Vector3 (-88, height, 0),Quaternion.identity);
            Destroy(c,cloudLifeSpan);
        }
        else if (chance == 2)
        {
            c = Instantiate(cc3,new Vector3 (-88, height, 0),Quaternion.identity);
            Destroy(c,cloudLifeSpan);
        }
    }
    void makeCustomCloud(float cloudposx){
        int chance = Random.Range(0,4);
        float height = Random.Range(1.8f,4.25f);
        if (chance == 0){
            c = Instantiate(cc1,new Vector3 (cloudposx, height, 0),Quaternion.identity);
            Destroy(c,cloudLifeSpan);
        }
        else if (chance == 1){
            c = Instantiate(cc2,new Vector3 (cloudposx, height, 0),Quaternion.identity);
            Destroy(c,cloudLifeSpan);
        }
        else if (chance == 2)
        {
            c = Instantiate(cc3,new Vector3 (cloudposx, height, 0),Quaternion.identity);
            Destroy(c,cloudLifeSpan);
        }
    }
}
