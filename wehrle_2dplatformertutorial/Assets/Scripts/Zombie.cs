﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    // Start is called before the first frame update
    float speed = .02f;
    float jump;
    float timer = 0.0f;
    float jumpfrequency;
    public Animator animator;
    GameObject player;
    Transform zomTransform;
    Rigidbody2D zomRigidbody;
    Transform playerTransform;
    
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        zomRigidbody = GetComponent<Rigidbody2D>();
        zomTransform = GetComponent<Transform>();
        jump = Random.Range(490f,510f);
        speed = Random.Range(5f,7f);
        jumpfrequency = Random.Range(5.5f,7f);
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        //this is the jump
        if (player != null){
            if (timer > jumpfrequency){
                animator.SetTrigger("Attack");
                if (playerTransform.position.x > zomTransform.position.x){
                    zomRigidbody.AddForce(new Vector3(400f,jump,0f)* Time.deltaTime*60,ForceMode2D.Force);
                }
                else
                {
                    zomRigidbody.AddForce(new Vector3(-400f,jump,0f) * Time.deltaTime * 60, ForceMode2D.Force);
                }
        
                timer = 0f;
            }
        }
        //this is the silly movement
        if (player != null){
            
            if (playerTransform.position.x > zomTransform.position.x){
                zomRigidbody.AddForce(new Vector3(speed,0f,0f) * Time.deltaTime * 60, ForceMode2D.Force);
            }
            else
            {
                zomRigidbody.AddForce(new Vector3(-speed,0f,0f) * Time.deltaTime * 60, ForceMode2D.Force);
            } 
            
            /* OLD Version
            if (playerTransform.position.x > zomTransform.position.x){
                zomTransform.position = new Vector3 (zomTransform.position.x + speed,zomTransform.position.y ,zomTransform.position.z );
            }
            else
            {
                zomTransform.position = new Vector3 (zomTransform.position.x - speed,zomTransform.position.y ,zomTransform.position.z );
            }
             */
        }
    }
    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Player"){
            Destroy(other.gameObject);
        }
    }
}
