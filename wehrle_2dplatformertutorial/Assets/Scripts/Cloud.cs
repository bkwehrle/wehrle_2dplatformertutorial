﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    // Start is called before the first frame update
    Transform t;
    void Awake()
    {
        
        t = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 pos = t.position;
        pos.x = t.position.x + Time.deltaTime/8;
        t.position = pos;
    }
}
