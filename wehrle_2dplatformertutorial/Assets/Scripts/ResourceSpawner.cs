﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResourceSpawner : MonoBehaviour
{
    // This script spawns all the wood, bats, and zombies each day.
    public Timer t;
    int currentday=0;
    public GameObject wood;
    public GameObject bat;
    public GameObject zombie;
    GameObject w;
    void Start()
    {
        t = t.GetComponent<Timer>();
        Random.InitState(System.DateTime.Now.Millisecond);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("TilemapStart");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (t.day > currentday)
        {
            //again i hard coded the bounds. Could fix later if i find myself changing the world size.
            for (int i = -87; i < 82; i++)
            {
                if (!((i > -19)&& (i < -3))) { 
                    //wood
                    int chance = Random.Range(0, 8);
                    if (chance == 0)
                    {
                        w = Instantiate(wood, new Vector3(i +.5f, 6, 0), Quaternion.identity);
                        w.SetActive(true);
                    }
                    //bats
                    chance = Random.Range(0, 14);
                    if (chance == 0)
                    {
                        Instantiate(bat, new Vector3(i +.5f, 7, 0), Quaternion.identity);
                    }
                    //zombies
                    chance = Random.Range(0,28);
                    if (chance == 0){
                        Instantiate(zombie, new Vector3(i +.5f, 7, 0), Quaternion.identity);
                    }
                }
            }
            currentday += 1;
        }
    }
}
