﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    Transform player;
    void Awake()
    {
        //FindGameObjectWithTag is awesome.
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {   
        if (player!=null){
            Vector3 moveT = player.position;
            moveT.y += .5f;
            transform.position = Vector2.MoveTowards(transform.position,moveT,speed*Time.deltaTime);
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player"){
            Destroy(other.gameObject);
        }
    }
}
