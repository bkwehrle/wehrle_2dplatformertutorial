﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wood : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D rb;
    public Timer timerScript;
    //public GameObject timer;
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        timerScript = timerScript.GetComponent<Timer>();
    }

    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Player"){
            Destroy(gameObject);
            timerScript.woodPlusOne();
        }
    }
}
