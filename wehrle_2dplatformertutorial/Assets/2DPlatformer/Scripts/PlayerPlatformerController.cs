﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController : PhysicsObject
{
    public float jumpTakeOffSpeed = 7f;
    public float maxSpeed = 7;
    // Start is called before the first frame update

    //we want to flip the sprite based on which way she is moving
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    Transform t;
    public PlaceTiles placeTilesScript;
    public Timer timer;

    void Awake(){
        placeTilesScript = placeTilesScript.GetComponent<PlaceTiles>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        t = GetComponent<Transform>();
        animator = GetComponent<Animator>();
        timer = timer.GetComponent<Timer>();
    }
    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump")&& grounded)
        {
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }
        if (Input.GetButtonDown("Action"))
        {            
            //trying to throw together a boxcast like ian told me to.
            //this works well enough that i am happy with it.
            RaycastHit2D boxRayHit2d;
            Vector3 cratepos = gameObject.transform.position;
            cratepos.x -= .8f;
            cratepos.y += 0.7f;
            boxRayHit2d = Physics2D.BoxCast(cratepos,new Vector2(.5f,.5f),0f,new Vector2(-.5f,.5f),0.25f);
            if (boxRayHit2d.collider == null){
                if (timer.wood >= 3){
                timer.wood -= 3;
                //print("here");
                cratepos.x += .8f;
                cratepos.y -= .7f;
                placeTilesScript.PlaceCrate(cratepos);
                }
            }
            
        }

        //after we have figured out the movement we check if we need to flip the sprite

        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f):(move.x<0.01f));
        if (flipSprite){
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool("grounded",grounded);
        animator.SetFloat("velocityX",Mathf.Abs(velocity.x)/maxSpeed);

        targetVelocity = move * maxSpeed;
    }



}
