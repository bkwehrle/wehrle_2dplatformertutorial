﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{

    public float minGroundNormalY = .65f;

    //So we can scale the gravity as we go; see what feels right
    public float gravityModifier = 1f;

    //incoming input from outside the class
    protected Vector2 targetVelocity;
    protected bool grounded;
    //minimum ground normal
    protected Vector2 groundNormal;
    protected Rigidbody2D rb2d;
    protected Vector2 velocity;
    protected ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);


    protected const float minMoveDistance = 0.001f;
    protected const float shellRadius = 0.01f;

    void OnEnable()
    {
        //good practice to get component here or in Awake
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        //dont check collisions against triggers
        contactFilter.useTriggers = false;
        //were getting a layer mask from the project settings from physics 2d
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        //now we could pass this to rb2b!
        contactFilter.useLayerMask = true;
        //groundNormal = new Vector2(0f, 1f);
    }

    void Update()
    {
        targetVelocity = Vector2.zero;
        ComputeVelocity();
    }

    protected virtual void ComputeVelocity()
    {

    }

    void FixedUpdate()
    {
        //using our "scaled" gravity so we can apply to the object.
        velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
        //updating the velocity.x accordingly with our input velocity
        velocity.x = targetVelocity.x;

        grounded = false;

        //our new position
        //This youtube comment explains this line well:
        //The reason he is multiplying velocity by deltaTime is because in physics,
        //velocity = distance / time. So, using simple algebra, we get distance = velocity * time.
        Vector2 deltaPosition = velocity * Time.deltaTime;

        //
        Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);

        //what we pass to the movement function. Vertical movement.
        
        //to deal with slopes
        Vector2 move = moveAlongGround * deltaPosition.x;

        Movement(move, false);

        move = Vector2.up * deltaPosition.y;

        Movement(move, true);
    }

    void Movement(Vector2 move, bool yMovement)
    {
        //storing this to see if we are moving at all so we dont have to check collisions all the time.
        float distance = move.magnitude;

        if (distance > minMoveDistance)
        {   
            //we added shellRadius as a small bit of padding so we dont clip through.
            int count = rb2d.Cast(move, contactFilter, hitBuffer, distance + shellRadius);
            hitBufferList.Clear();
            //so we dont use old data

            //each entry from the array is gonna get copied over from the HitBufferList
            for (int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }
            //compare the normal to a minimum ground normal value
            // got to 3:36 of 4 / 8
            for (int i = 0; i < hitBufferList.Count; i++)
            {
                //trying to determine if the player is grounded (so we can see if they should animate still or falling animation)
                Vector2 currentNormal = hitBufferList[i].normal;
                if (currentNormal.y > minGroundNormalY)
                {
                    //we're grounded!
                    grounded = true;
                    //for slopes
                    if (yMovement)
                    {
                        groundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }
                //getting the difference between the velocity and currentnormal and determining whether we need to subtract the velocity
                //so the player doesnt enter another collider.
                float projection = Vector2.Dot(velocity, currentNormal);
                if (projection < 0)
                {   
                    velocity = velocity - projection * currentNormal;
                }
                //so we dont get stuck
                float modifiedDistance = hitBufferList[i].distance - shellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }


        }
        //we can set the position of our rb2d with our advanced distance.
        rb2d.position = rb2d.position + move.normalized * distance;
    }

}